package pl.sda.todos.util;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class CryptUtilTest {

	@Test
	public void testPasswordOperation() {
		// given
		String password = "tajneHaslo2020!";

		// when
		String hashPassword = CryptUtil.hashPassword(password);

		// then
		assertThat(CryptUtil.checkPassword(password, hashPassword), is(true));
	}

}