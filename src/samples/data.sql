CREATE SCHEMA `todos` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_polish_ci ;

CREATE DATABASE `todos` DEFAULT CHARACTER SET utf8 COLLATE utf8_polish_ci;


CREATE USER `user_todos`@`localhost` IDENTIFIED BY `todos01`;

GRANT ALL PRIVILEGES ON todos.* TO `user_todos`@`localhost`;

FLUSH PRIVILEGES;

INSERT INTO `Task` (`id`,`description`,`title`,`user_id`,`priority`,`createdDate`,`modifiedDate`,`status`,`loggedWork`) VALUES (1,'trzeba wreszcie ogarnąć pranie','Pranie',80,3,'2020-01-24 13:01:35.355185','2020-01-24 13:01:35.355185','TODO',180);
INSERT INTO `Task` (`id`,`description`,`title`,`user_id`,`priority`,`createdDate`,`modifiedDate`,`status`,`loggedWork`) VALUES (2,'posprzątaj!','Sprzątanie',81,0,'2020-01-24 13:01:35.415330','2020-01-24 13:01:35.415330','DOING',30);
INSERT INTO `Task` (`id`,`description`,`title`,`user_id`,`priority`,`createdDate`,`modifiedDate`,`status`,`loggedWork`) VALUES (3,'ugotować','Gotowanie',81,2,'2020-01-24 13:01:35.417684','2020-01-24 13:01:35.417684','DONE',30);
INSERT INTO `Task` (`id`,`description`,`title`,`user_id`,`priority`,`createdDate`,`modifiedDate`,`status`,`loggedWork`) VALUES (4,'w weekend zakupy','Zakupy',81,0,'2020-01-24 13:01:35.418766','2020-01-24 13:01:35.418766','TODO',0);
INSERT INTO `Task` (`id`,`description`,`title`,`user_id`,`priority`,`createdDate`,`modifiedDate`,`status`,`loggedWork`) VALUES (5,'odkurzacz i jedziesz','Odkurzanie',81,4,'2020-01-24 13:01:35.419877','2020-01-24 13:01:35.419877','TODO',120);
INSERT INTO `Task` (`id`,`description`,`title`,`user_id`,`priority`,`createdDate`,`modifiedDate`,`status`,`loggedWork`) VALUES (6,'koszenie trawy','Ogródek',81,1,'2020-01-24 13:01:35.421501','2020-01-24 13:01:35.421501','TODO',0);
INSERT INTO `Task` (`id`,`description`,`title`,`user_id`,`priority`,`createdDate`,`modifiedDate`,`status`,`loggedWork`) VALUES (7,'wynieść stare opony','Garaż',81,3,'2020-01-24 13:01:35.423350','2020-01-24 13:01:35.423350','TODO',90);
INSERT INTO `Task` (`id`,`description`,`title`,`user_id`,`priority`,`createdDate`,`modifiedDate`,`status`,`loggedWork`) VALUES (8,'do 10ego wszystko opłacić','Opłaty',81,4,'2020-01-24 13:01:35.425860','2020-01-24 13:01:35.425860','DOING',60);
INSERT INTO `Task` (`id`,`description`,`title`,`user_id`,`priority`,`createdDate`,`modifiedDate`,`status`,`loggedWork`) VALUES (9,'zabookować egzotyk','Wycieczka',80,2,'2020-01-24 13:01:35.427789','2020-01-24 13:01:35.427789','DONE',0);
INSERT INTO `Task` (`id`,`description`,`title`,`user_id`,`priority`,`createdDate`,`modifiedDate`,`status`,`loggedWork`) VALUES (10,'omówmy się na weekend ze znajomymi','Spotkanie',80,3,'2020-01-24 13:01:35.430730','2020-01-24 13:01:35.430730','TODO',90);
INSERT INTO `Task` (`id`,`description`,`title`,`user_id`,`priority`,`createdDate`,`modifiedDate`,`status`,`loggedWork`) VALUES (11,'3 kosze do wyniesienia','Wyrzucić śmieci',80,0,'2020-01-24 13:01:35.431942','2020-01-24 13:01:35.431942','DONE',0);