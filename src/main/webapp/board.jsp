<%@ include file="fragments/header.jspf"%>

    <title>Board</title>
</head>
<body>

<div class="container">
    <%@ include file="fragments/menu.jspf"%>
    <%@ include file="fragments/message.jspf"%>
    <button type="button" class="btn btn-primary btn-lg btn-block">Board</button>

    <div class="row">
        <div class="col"><h3>TODO</h3></div>
        <div class="col"><h3>DOING</h3></div>
        <div class="col"><h3>DONE</h3></div>
        <div class="w-100"></div>
        <div class="col">
            <!-- tu bedzie lista zadan TODO -->
            <div class="list-group">
                <c:set var="count" value="0"/>
                <c:set var="color" value=""/>
                <c:forEach items="${todo}" var="item">
                    <c:choose>
                        <c:when test="${item.priority.value == 1}">
                            <c:set var="color" value="danger"/>
                        </c:when>
                        <c:when test="${item.priority.value == 2}">
                            <c:set var="color" value="warning"/>
                        </c:when>
                        <c:when test="${item.priority.value == 3}">
                            <c:set var="color" value="info"/>
                        </c:when>
                        <c:when test="${item.priority.value == 4}">
                            <c:set var="color" value="secondary"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="color" value="light"/>
                        </c:otherwise>
                    </c:choose>
                    <div class="list-group-item list-group-item-action list-group-item-${color}">
                        <div class="d-flex w-100 justify-content-between align-items-center">
                            <h5 class="mb-1"><a href="<c:url value="/edittask"><c:param name="id" value="${item.id}"/></c:url>" class="list-group-item-action list-group-item-${color}">${item.title}</a></h5>
                            <span class="badge badge-primary badge-pill">${tf.toHours(item.loggedWork)}</span>
                        </div>
                        <small>${item.description}</small>
                        <div><span class="badge badge-secondary">${item.user.login}</span></div>
                        <c:if test="${loggedUserId eq item.user.id}">
                            <div class="d-flex justify-content-end">
                                <a href="<c:url value="/transit"><c:param name="id" value="${item.id}"/><c:param name="to" value="DOING"/></c:url>" class="badge badge-pill badge-success">--&gt;</a>
                            </div>
                        </c:if>
                    </div>
                    <c:set var="count" value="${count + item.loggedWork}"/>
                </c:forEach>
                <div>
                    <button type="button" class="btn btn-primary">
                        Suma <span class="badge badge-light">${tf.toHours(count)}</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="list-group">
                <c:set var="count" value="0"/>
                <c:set var="color" value=""/>
                <c:forEach items="${doing}" var="item">
                    <c:choose>
                        <c:when test="${item.priority.value == 1}">
                            <c:set var="color" value="danger"/>
                        </c:when>
                        <c:when test="${item.priority.value == 2}">
                            <c:set var="color" value="warning"/>
                        </c:when>
                        <c:when test="${item.priority.value == 3}">
                            <c:set var="color" value="info"/>
                        </c:when>
                        <c:when test="${item.priority.value == 4}">
                            <c:set var="color" value="secondary"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="color" value="light"/>
                        </c:otherwise>
                    </c:choose>
                    <div class="list-group-item list-group-item-action list-group-item-${color}">
                        <div class="d-flex w-100 justify-content-between align-items-center">
                            <h5 class="mb-1"><a href="<c:url value="/edittask"><c:param name="id" value="${item.id}"/></c:url>" class="list-group-item-action list-group-item-${color}">${item.title}</a></h5>
                            <span class="badge badge-primary badge-pill">${tf.toHours(item.loggedWork)}</span>
                        </div>
                        <small>${item.description}</small>
                        <div><span class="badge badge-secondary">${item.user.login}</span></div>
                        <c:if test="${loggedUserId eq item.user.id}">
                            <div class="d-flex justify-content-between">
                                <a href="<c:url value="/transit"><c:param name="id" value="${item.id}"/><c:param name="to" value="TODO"/></c:url>" class="badge badge-pill badge-danger">&lt;--</a>
                                <a href="<c:url value="/transit"><c:param name="id" value="${item.id}"/><c:param name="to" value="DONE"/></c:url>" class="badge badge-pill badge-success">--&gt;</a>
                            </div>
                        </c:if>
                    </div>
                    <c:set var="count" value="${count + item.loggedWork}"/>
                </c:forEach>
                <div>
                    <button type="button" class="btn btn-primary">
                        Suma <span class="badge badge-light">${tf.toHours(count)}</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="list-group">
                <c:set var="count" value="0"/>
                <c:set var="color" value=""/>
                <c:forEach items="${done}" var="item">
                    <c:choose>
                        <c:when test="${item.priority.value == 1}">
                            <c:set var="color" value="danger"/>
                        </c:when>
                        <c:when test="${item.priority.value == 2}">
                            <c:set var="color" value="warning"/>
                        </c:when>
                        <c:when test="${item.priority.value == 3}">
                            <c:set var="color" value="info"/>
                        </c:when>
                        <c:when test="${item.priority.value == 4}">
                            <c:set var="color" value="secondary"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="color" value="light"/>
                        </c:otherwise>
                    </c:choose>
                    <div class="list-group-item list-group-item-action list-group-item-${color}">
                        <div class="d-flex w-100 justify-content-between align-items-center">
                            <h5 class="mb-1"><a href="<c:url value="/edittask"><c:param name="id" value="${item.id}"/></c:url>" class="list-group-item-action list-group-item-${color}">${item.title}</a></h5>
                            <span class="badge badge-primary badge-pill">${tf.toHours(item.loggedWork)}</span>
                        </div>
                        <small>${item.description}</small>
                        <div><span class="badge badge-secondary">${item.user.login}</span></div>
                        <c:if test="${loggedUserId eq item.user.id}">
                            <div class="d-flex justify-content-start">
                                <a href="<c:url value="/transit"><c:param name="id" value="${item.id}"/><c:param name="to" value="DOING"/></c:url>" class="badge badge-pill badge-danger">&lt;--</a>
                            </div>
                        </c:if>
                    </div>
                    <c:set var="count" value="${count + item.loggedWork}"/>
                </c:forEach>
                <div>
                    <button type="button" class="btn btn-primary">
                        Suma <span class="badge badge-light">${tf.toHours(count)}</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<%@ include file="fragments/footer.jspf"%>