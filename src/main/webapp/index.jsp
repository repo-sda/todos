<%@ include file="fragments/header.jspf"%>

<title>Logowanie</title>
</head>
<body>

<div class="container">
<%--    <%@ include file="fragments/menu.jspf"%>--%>
    <%@ include file="fragments/message.jspf"%>
    <button type="button" class="btn btn-primary btn-lg btn-block">Logowanie</button>

    <form action="/login" method="post">
        <div class="form-group">
            <label for="login">Login</label>
            <input name="login" type="text" class="form-control" id="login" aria-describedby="loginHelp" value="${param.login}">
            <small id="loginHelp" class="form-text text-muted">Podaj swój login.</small>
        </div>
        <div class="form-group">
            <label for="password">Hasło</label>
            <input name="password" type="password" class="form-control" id="password" aria-describedby="passwordHelp">
            <small id="passwordHelp" class="form-text text-muted">Podaj swoje hasło.</small>
        </div>
        <button type="submit" class="btn btn-primary">Zaloguj</button>
    </form>

</div>

<%@ include file="fragments/footer.jspf"%>