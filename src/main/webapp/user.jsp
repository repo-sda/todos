<%@ include file="fragments/header.jspf"%>

    <title>User</title>
</head>
<body>

<div class="container">
    <%@ include file="fragments/menu.jspf"%>
    <%@ include file="fragments/message.jspf"%>
    <button type="button" class="btn btn-primary btn-lg btn-block">Użytkownik</button>

    <form>
        <input type="hidden" name="id" value="${user.id}">
        <div class="form-group">
            <label for="login">Login</label>
            <input type="text" class="form-control" id="login" aria-describedby="loginHelp" value="${user.login}">
            <small id="loginHelp" class="form-text text-muted">Podaj unikatowy login.</small>
        </div>
        <div class="form-group">
            <label for="name">Imię</label>
            <input type="text" class="form-control" id="name" value="${user.name}">
        </div>
        <div class="form-group">
            <label for="name">Nazwisko</label>
            <input type="text" class="form-control" id="surname" value="${user.surname}">
        </div>
        <div class="form-group">
            <label for="name">Data utworzenia</label>
            <input type="text" class="form-control" id="createdDate" value="<jt:format style="SM" value="${user.createdDate}"/>" readonly>
        </div>
        <div class="form-group">
            <label for="name">Data modyfikacji</label>
            <input type="text" class="form-control" id="modifiedDate" value="<jt:format style="SM" value="${user.modifiedDate}"/>" readonly>
        </div>
        <button type="submit" class="btn btn-primary">Edytuj</button>
    </form>
    <br clear="all">
    <c:if test="${loggedUserId eq user.id}">
        <button type="button" class="btn btn-primary btn-lg btn-block">Administracja</button>
        <form action="/logout" method="post">
            <button type="submit" class="btn btn-danger">wyloguj</button>
        </form>
    </c:if>

</div>

<%@ include file="fragments/footer.jspf"%>