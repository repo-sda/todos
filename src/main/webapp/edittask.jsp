<%@ include file="fragments/header.jspf"%>

<title>Edit Task</title>
</head>
<body>

<div class="container">
    <%@ include file="fragments/menu.jspf"%>
    <%@ include file="fragments/message.jspf"%>
    <button type="button" class="btn btn-primary btn-lg btn-block">Edycja Zadania</button>

    <form action="/edittask" method="post">
        <input type="hidden" name="id" value="${task.id}">
        <div class="form-group">
            <label for="title">Tytuł</label>
            <input name="title" type="text" class="form-control" id="title" aria-describedby="titleHelp" value="${task.title}">
            <small id="titleHelp" class="form-text text-muted">Podaj krótką nazwę zadania.</small>
        </div>

        <div class="form-group">
            <label for="description">Opis</label>
            <textarea name="description" class="form-control" id="description" aria-describedby="descriptionHelp" rows="3">${task.description}</textarea>
            <small id="descriptionHelp" class="form-text text-muted">Podaj opis zadania.</small>
        </div>

        <div class="form-group">
            <label for="priority">Priorytet</label>
            <select name="priority" class="form-control" id="priority" aria-describedby="priorityHelp">
                <c:forEach begin="${from}" end="${to}" var="i">
                    <c:set var="selected" value=""/>
                    <c:if test="${task.priority.value eq i}">
                        <c:set var="selected" value="selected"/>
                    </c:if>
                    <option value="${i}" ${selected}>${i}</option>
                </c:forEach>
            </select>
            <small id="priorityHelp" class="form-text text-muted">Podaj priorytet zadania.</small>
        </div>

        <div class="form-group">
            <label for="user">Użytkownik</label>
            <select name="user" class="form-control" id="user" aria-describedby="userHelp">
                <c:forEach items="${users}" var="user">
                    <c:set var="selected" value=""/>
                    <c:if test="${task.user.id eq user.id}">
                        <c:set var="selected" value="selected"/>
                    </c:if>
                    <option value="${user.id}" ${selected}>${user.name} ${user.surname} (${user.login})</option>
                </c:forEach>
            </select>
            <small id="userHelp" class="form-text text-muted">Przypisz zadanie użytkownikowi.</small>
        </div>

        <div class="form-group">
            <label for="loggedWork">Zalogowany czas</label>
            <input type="text" class="form-control" id="loggedWork" value="${tf.toHours(task.loggedWork)}" readonly>
        </div>

        <div class="form-group">
            <label for="createdDate">Data utworzenia</label>
            <input type="text" class="form-control" id="createdDate" value="<jt:format style="SM" value="${task.createdDate}"/>" readonly>
        </div>
        <div class="form-group">
            <label for="modifiedDate">Data modyfikacji</label>
            <input type="text" class="form-control" id="modifiedDate" value="<jt:format style="SM" value="${task.modifiedDate}"/>" readonly>
        </div>

        <button type="submit" class="btn btn-primary">Edytuj</button>
    </form>
    <br clear="all">
    <c:if test="${loggedUserId eq task.user.id}">
        <button type="button" class="btn btn-primary btn-lg btn-block">Logowanie czasu</button>
        <form>
            <div class="form-group">
                <label for="logWork">Zaloguj czas</label>
                <select name="logWork" class="form-control" id="logWork" aria-describedby="logWorkHelp">
                    <c:forEach begin="30" end="1080" var="i" step="30">
                        <option value="${i}">${tf.toHours(i)}</option>
                    </c:forEach>
                </select>
                <small id="logWorkHelp" class="form-text text-muted">Dodaj czas, który poświęciłeś na zadanie.</small>
            </div>

            <button type="submit" class="btn btn-primary">Dodaj</button>
        </form>
    </c:if>

</div>

<%@ include file="fragments/footer.jspf"%>