<%@ include file="fragments/header.jspf"%>

    <title>Users</title>
</head>
<body>

<div class="container">
    <%@ include file="fragments/menu.jspf"%>
    <%@ include file="fragments/message.jspf"%>
    <button type="button" class="btn btn-primary btn-lg btn-block">Użytkownicy</button>

    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">login</th>
                <th scope="col">name</th>
                <th scope="col">surname</th>
                <th scope="col">created date</th>
                <th scope="col">modified date</th>
            </tr>
        </thead>
            <c:forEach items="${users}" var="u">
                <tr>
                    <td><a href="<c:url value="/user"><c:param name="id" value="${u.id}"/></c:url>">${u.login}</a></td>
                    <td><a href="<c:url value="/user"><c:param name="id" value="${u.id}"/></c:url>">${u.name}</a></td>
                    <td><a href="<c:url value="/user"><c:param name="id" value="${u.id}"/></c:url>">${u.surname}</a></td>
                    <td><jt:format style="SM" value="${u.createdDate}"/></td>
                    <td><jt:format style="SM" value="${u.modifiedDate}"/></td>
                </tr>
            </c:forEach>
    </table>
</div>

<%@ include file="fragments/footer.jspf"%>