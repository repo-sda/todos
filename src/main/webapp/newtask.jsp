<%@ include file="fragments/header.jspf"%>

<title>New Task</title>
</head>
<body>

<div class="container">
    <%@ include file="fragments/menu.jspf"%>
    <%@ include file="fragments/message.jspf"%>
    <button type="button" class="btn btn-primary btn-lg btn-block">Nowe Zadanie</button>

    <form action="/addtask" method="post">
        <div class="form-group">
            <label for="title">Title</label>
            <input name="title" type="text" class="form-control" id="title" aria-describedby="titleHelp">
            <small id="titleHelp" class="form-text text-muted">Podaj krótką nazwę zadania.</small>
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <textarea name="description" class="form-control" id="description" aria-describedby="descriptionHelp" rows="3"></textarea>
            <small id="descriptionHelp" class="form-text text-muted">Podaj opis zadania.</small>
        </div>

        <div class="form-group">
            <label for="priority">Priority</label>
            <select name="priority" class="form-control" id="priority" aria-describedby="priorityHelp">
                <c:forEach begin="${from}" end="${to}" var="i">
                    <option value="${i}">${i}</option>
                </c:forEach>
            </select>
            <small id="priorityHelp" class="form-text text-muted">Podaj priorytet zadania.</small>
        </div>

        <div class="form-group">
            <label for="user">User</label>
            <select name="user" class="form-control" id="user" aria-describedby="userHelp">
                <c:forEach items="${users}" var="user">
                    <option value="${user.id}">${user.name} ${user.surname} (${user.login})</option>
                </c:forEach>
            </select>
            <small id="userHelp" class="form-text text-muted">Przypisz zadanie użytkownikowi.</small>
        </div>

        <button type="submit" class="btn btn-primary">Dodaj</button>
    </form>

</div>

<%@ include file="fragments/footer.jspf"%>