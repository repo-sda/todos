<%@ include file="fragments/header.jspf"%>

<title>New User</title>
</head>
<body>

<div class="container">
    <%@ include file="fragments/menu.jspf"%>
    <%@ include file="fragments/message.jspf"%>
    <button type="button" class="btn btn-primary btn-lg btn-block">Nowy użytkownik</button>

    <form action="/adduser" method="post">
        <div class="form-group">
            <label for="login">Login</label>
            <input name="login" type="text" class="form-control" id="login" aria-describedby="loginHelp" value="${param.login}">
            <small id="loginHelp" class="form-text text-muted">Podaj unikatowy login.</small>
        </div>
        <div class="form-group">
            <label for="password">Hasło</label>
            <input name="password" type="password" class="form-control" id="password" aria-describedby="passwordHelp">
            <small id="passwordHelp" class="form-text text-muted">Podaj hasło.</small>
        </div>
        <div class="form-group">
            <label for="password2">Hasło (powtórzone)</label>
            <input name="password2" type="password" class="form-control" id="password2" aria-describedby="password2Help">
            <small id="password2Help" class="form-text text-muted">Powtórz hasło.</small>
        </div>
        <div class="form-group">
            <label for="name">Imię</label>
            <input name="name" type="text" class="form-control" id="name" aria-describedby="nameHelp" value="${param.name}">
            <small id="nameHelp" class="form-text text-muted">Podaj imię.</small>
        </div>
        <div class="form-group">
            <label for="name">Nazwisko</label>
            <input name="surname" type="text" class="form-control" id="surname" aria-describedby="surnameHelp" value="${param.surname}">
            <small id="surnameHelp" class="form-text text-muted">Podaj nazwisko.</small>
        </div>
        <button type="submit" class="btn btn-primary">Dodaj</button>
    </form>

</div>

<%@ include file="fragments/footer.jspf"%>