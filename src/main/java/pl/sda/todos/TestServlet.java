package pl.sda.todos;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.mindrot.jbcrypt.BCrypt;
import pl.sda.todos.domain.Priority;
import pl.sda.todos.domain.Status;
import pl.sda.todos.domain.Task;
import pl.sda.todos.domain.User;
import pl.sda.todos.repository.FakeTaskRepository;
import pl.sda.todos.repository.FakeUserRepository;
import pl.sda.todos.repository.TaskRepository;
import pl.sda.todos.repository.UserRepository;
import pl.sda.todos.util.HibernateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@WebServlet(name = "test", value = "/test")
public class TestServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		SessionFactory sf = HibernateUtil.getSessionFactory();
		System.out.println("sf = " + sf);
		
		//SessionFactory sf = (SessionFactory) getServletContext().getAttribute("SessionFactory");
		//Transaction transaction = sf.getCurrentSession().beginTransaction();

		//UserRepository userRepository = new UserRepository(sf);
		//TaskRepository taskRepository = new TaskRepository(sf);
		FakeTaskRepository taskRepository = new FakeTaskRepository();
		FakeUserRepository userRepository = new FakeUserRepository();

		User byId80 = userRepository.getById(80);
		User byId81 = userRepository.getById(81);

		Task task1 = new Task("Pranie", "trzeba wreszcie ogarnąć pranie", Priority.FOUR, Status.TODO, byId80);
		Task task2 = new Task("Sprzątanie", "posprzątaj!", Priority.ONE, Status.DOING, byId81);
		Task task3 = new Task("Gotowanie", "ugotować", Priority.THREE, Status.DONE, byId81);
		Task task4 = new Task("Zakupy", "w weekend zakupy", Priority.ONE, Status.TODO, byId81);
		Task task5 = new Task("Odkurzanie", "odkurzacz i jedziesz", Priority.FIVE, Status.TODO, byId81);
		Task task6 = new Task("Ogródek", "koszenie trawy", Priority.TWO, Status.TODO, byId81);
		Task task7 = new Task("Garaż", "wynieść stare opony", Priority.FOUR, Status.TODO, byId81);
		Task task8 = new Task("Opłaty", "do 10ego wszystko opłacić", Priority.FIVE, Status.DOING, byId81);
		Task task9 = new Task("Wycieczka", "zabookować egzotyk", Priority.THREE, Status.DONE, byId80);
		Task task10 = new Task("Spotkanie", "omówmy się na weekend ze znajomymi", Priority.FOUR, Status.TODO, byId80);
		Task task11 = new Task("Wyrzucić śmieci", "3 kosze do wyniesienia", Priority.ONE, Status.DONE, byId80);


		//taskRepository.save(task1);
		//taskRepository.save(task2);
		//taskRepository.save(task3);
		//taskRepository.save(task4);
		//taskRepository.save(task5);
		//taskRepository.save(task6);
		//taskRepository.save(task7);
		//taskRepository.save(task8);
		//taskRepository.save(task9);
		//taskRepository.save(task10);
		//taskRepository.save(task11);

		//transaction.commit();
		//sf.getCurrentSession().close();

	}
}
