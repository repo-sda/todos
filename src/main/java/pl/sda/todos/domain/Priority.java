package pl.sda.todos.domain;

public enum Priority {
	ONE(1),
	TWO(2),
	THREE(3),
	FOUR(4),
	FIVE(5);

	private int value;

	Priority(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public static Priority get(int value) {
		return Priority.values()[value - 1];
	}
}
