package pl.sda.todos.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString(exclude = {"password", "tasks"})
@EqualsAndHashCode(of = {"id"})
@Table(
		uniqueConstraints = {
				@UniqueConstraint(columnNames = {"login"})
		}
)
@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String login;
	private String password;
	private String name;
	private String surname;
	@Column(updatable = false)
	private LocalDateTime createdDate;
	private LocalDateTime modifiedDate;

	@OneToMany(mappedBy = "user")
	private List<Task> tasks = new ArrayList<>();


	public User() {
	}

	public User(String login, String password, String name, String surname) {
		this.login = login;
		this.password = password;
		this.name = name;
		this.surname = surname;
	}

	@PrePersist
	public void prePersist() {
		createdDate = modifiedDate = LocalDateTime.now();
	}

	@PreUpdate
	public void preUpdate() {
		modifiedDate = LocalDateTime.now();
	}

	public User addTask(Task task) {
		tasks.add(task);
		return this;
	}
}
