package pl.sda.todos.domain;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum Status {
	TODO {
		@Override
		public boolean isTransitiableTo(Status status) {
			return status == DOING;
		}

		@Override
		public List<Status> getTransitiableStates() {
			return Arrays.asList(DOING);
		}
	},
	DOING {
		@Override
		public boolean isTransitiableTo(Status status) {
			return status == TODO || status == DONE;
		}

		@Override
		public List<Status> getTransitiableStates() {
			return Arrays.asList(TODO, DONE);
		}
	},
	DONE {
		@Override
		public boolean isTransitiableTo(Status status) {
			return status == DOING || status == CLOSED;
		}

		@Override
		public List<Status> getTransitiableStates() {
			return Arrays.asList(DOING, CLOSED);
		}
	},
	CLOSED {
		@Override
		public boolean isTransitiableTo(Status status) {
			return false;
		}

		@Override
		public List<Status> getTransitiableStates() {
			return Collections.emptyList();
		}
	};

	public abstract boolean isTransitiableTo(Status status);
	public abstract List<Status> getTransitiableStates();
}
