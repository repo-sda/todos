package pl.sda.todos.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"id"})
@Entity
public class Task {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String title;
	private String description;
	@Enumerated(EnumType.ORDINAL)
	private Priority priority;
	@Enumerated(EnumType.STRING)
	private Status status;
	private int loggedWork;
	@Column(updatable = false)
	private LocalDateTime createdDate;
	private LocalDateTime modifiedDate;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;


	public Task() {
	}

	public Task(String title, String description, Priority priority, Status status, User user) {
		this.title = title;
		this.description = description;
		this.priority = priority;
		this.status = status;
		this.user = user;
	}

	public boolean isTransitiableTo(Status status) {
		return this.status.isTransitiableTo(status);
	}

	public void logWork(int minutes) {
		loggedWork += minutes;
	}

	@PrePersist
	public void prePersist() {
		createdDate = modifiedDate = LocalDateTime.now();
	}

	@PreUpdate
	public void preUpdate() {
		modifiedDate = LocalDateTime.now();
	}
}
