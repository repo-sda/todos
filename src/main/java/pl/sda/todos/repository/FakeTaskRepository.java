package pl.sda.todos.repository;

import org.hibernate.Session;
import pl.sda.todos.domain.Status;
import pl.sda.todos.domain.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FakeTaskRepository {

	// priority nie jest zachowane
	public List<Task> getAllByStatusOrderByPriority(Status status) {
		switch (status) {
			case TODO:
				return FakeDatabase.TODO_TASKS;
			case DOING:
				return FakeDatabase.DOING_TASKS;
			case DONE:
				return FakeDatabase.DONE_TASKS;
		}
		return new ArrayList<>();
	}

	public void addTaskToTodo(Task task) {
		FakeDatabase.TODO_TASKS.add(task);
	}

	public Task getById(int id) {
		List<Task> tasks = new ArrayList<>();
		tasks.addAll(FakeDatabase.TODO_TASKS);
		tasks.addAll(FakeDatabase.DOING_TASKS);
		tasks.addAll(FakeDatabase.DONE_TASKS);
		for (Task task : tasks) {
			if (task.getId() == id) {
				return task;
			}
		}
		return null;
	}

	public void save(Task task) {
		//TODO:
	}
}
