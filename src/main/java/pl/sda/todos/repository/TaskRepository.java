package pl.sda.todos.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.todos.domain.Status;
import pl.sda.todos.domain.Task;
import pl.sda.todos.domain.User;

import java.util.List;

public class TaskRepository {

	private final SessionFactory sf;

	public TaskRepository(SessionFactory sf) {
		this.sf = sf;
	}

	public void changeTaskStatus(Task task, Status status) {
		Session session = sf.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		Task taskFromBase = session.find(Task.class, task.getId());
		taskFromBase.setStatus(status);
		transaction.commit();
	}

	public List<Task> getAllByStatusOrderByPriority(Status status) {
		Session session = sf.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		List<Task> tasks = (List<Task>) session.createQuery("from Task where status = :status order by priority ASC").setParameter("status", status).list();
		transaction.commit();
		return tasks;
	}

	public void createTask(Task task) {
		Session session = sf.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.save(task);
		transaction.commit();
	}

	public Task getById(int id) {
		Session session = sf.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		Task task = session.find(Task.class, id);
		transaction.commit();
		return task;
	}
}
