package pl.sda.todos.repository;

import pl.sda.todos.domain.Priority;
import pl.sda.todos.domain.Status;
import pl.sda.todos.domain.Task;
import pl.sda.todos.domain.User;
import pl.sda.todos.util.CryptUtil;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FakeDatabase {

	public static final User JAN_KOWALSKI = new User("foo", CryptUtil.hashPassword("tajnehaslo"), "Jan", "Kowalski");
	public static final User TOMASZ_MALINOWSKI = new User("bar", CryptUtil.hashPassword("tajnehaslo"), "Tomasz", "Malinowski");

	public static final Task ZAKUPY = new Task("Zakupy", "w weekend zakupy", Priority.ONE, Status.TODO, JAN_KOWALSKI);
	public static final Task PRANIE = new Task("Pranie", "trzeba wreszcie ogarnąć pranie", Priority.FOUR, Status.TODO, JAN_KOWALSKI);
	public static final Task WYCIECZKA = new Task("Wycieczka", "zabookować egzotyk", Priority.THREE, Status.DONE, JAN_KOWALSKI);
	public static final Task SPOTKANIE = new Task("Spotkanie", "omówmy się na weekend ze znajomymi", Priority.FOUR, Status.TODO, JAN_KOWALSKI);
	public static final Task SMIECI = new Task("Wyrzucić śmieci", "3 kosze do wyniesienia", Priority.ONE, Status.DONE, JAN_KOWALSKI);
	public static final Task SPRZATANIE = new Task("Sprzątanie", "posprzątaj!", Priority.ONE, Status.DOING, TOMASZ_MALINOWSKI);
	public static final Task GOTOWANIE = new Task("Gotowanie", "ugotować", Priority.THREE, Status.DONE, TOMASZ_MALINOWSKI);
	public static final Task ODKURZANIE = new Task("Odkurzanie", "odkurzacz i jedziesz", Priority.FIVE, Status.TODO, TOMASZ_MALINOWSKI);
	public static final Task OGRODEK = new Task("Ogródek", "koszenie trawy", Priority.TWO, Status.TODO, TOMASZ_MALINOWSKI);
	public static final Task GARAZ = new Task("Garaż", "wynieść stare opony", Priority.FOUR, Status.TODO, TOMASZ_MALINOWSKI);
	public static final Task OPLATY = new Task("Opłaty", "do 10ego wszystko opłacić", Priority.FIVE, Status.DOING, TOMASZ_MALINOWSKI);

	public static final List<User> USERS = new ArrayList<>();
	public static final List<Task> TODO_TASKS = new ArrayList<>();
	public static final List<Task> DOING_TASKS = Arrays.asList(SPRZATANIE, OPLATY);
	public static final List<Task> DONE_TASKS = Arrays.asList(WYCIECZKA, SMIECI, GOTOWANIE);

	static {
		JAN_KOWALSKI.setCreatedDate(LocalDateTime.now());
		JAN_KOWALSKI.setModifiedDate(JAN_KOWALSKI.getCreatedDate());
		JAN_KOWALSKI.addTask(ZAKUPY).addTask(PRANIE).addTask(WYCIECZKA).addTask(SPOTKANIE).addTask(SMIECI);

		TOMASZ_MALINOWSKI.setCreatedDate(LocalDateTime.now());
		TOMASZ_MALINOWSKI.setModifiedDate(TOMASZ_MALINOWSKI.getCreatedDate());
		TOMASZ_MALINOWSKI.addTask(SPRZATANIE).addTask(GOTOWANIE).addTask(ODKURZANIE).addTask(OGRODEK).addTask(GARAZ).addTask(OPLATY);

		ZAKUPY.logWork(60);
		PRANIE.logWork(120);
		WYCIECZKA.logWork(30);
		SPOTKANIE.logWork(180);
		SMIECI.logWork(240);
		SPRZATANIE.logWork(90);
		GOTOWANIE.logWork(90);

		USERS.addAll(Arrays.asList(JAN_KOWALSKI, TOMASZ_MALINOWSKI));
		TODO_TASKS.addAll(Arrays.asList(ZAKUPY, PRANIE, SPOTKANIE, ODKURZANIE, OGRODEK, GARAZ));
	}


}
