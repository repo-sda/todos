package pl.sda.todos.repository;

import org.hibernate.Session;
import pl.sda.todos.domain.User;
import pl.sda.todos.util.CryptUtil;

import java.util.List;
import java.util.Optional;

public class FakeUserRepository {

	public User getById(int id) {
		for (User user : FakeDatabase.USERS) {
			if (user.getId() == id) {
				return user;
			}
		}
		return null;
	}

//	public void addUser(User user) {
//		FakeDatabase.USERS.add(user);
//	}

//	public Optional<User> findByLogin(String login) {
//		for (User user : FakeDatabase.USERS) {
//			if (user.getLogin().equals(login)) {
//				return Optional.of(user);
//			}
//		}
//		return Optional.empty();
//	}

	public List<User> getAll() {
		return FakeDatabase.USERS;
	}

	// kolejnosc nie bedzie zachowana
	public List<User> getAllOrderByCreatedDateDesc() {
		return FakeDatabase.USERS;
	}
}
