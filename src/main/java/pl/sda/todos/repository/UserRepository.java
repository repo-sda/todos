package pl.sda.todos.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.todos.domain.User;

import java.util.List;
import java.util.Optional;

public class UserRepository {

	private final SessionFactory sf;

	public UserRepository(SessionFactory sf) {
		this.sf = sf;
	}

	public User getById(int id) {
		Session session = sf.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		User user = session.find(User.class, id);
		transaction.commit();
		return user;
	}

	public List<User> getAll() {
		Session session = sf.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		List<User> list = (List<User>) session.createQuery("from User").list();
		transaction.commit();
		return list;
	}

	public List<User> getAllOrderByCreatedDateDesc() {
		Session session = sf.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		List<User> list = session.createQuery("from User order by createdDate DESC").list();
		transaction.commit();
		return list;
	}

	public Optional<User> findByLogin(String login) {
		Session session = sf.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		List<User> users = (List<User>) session.createQuery("from User where login = :login")
				.setParameter("login", login).list();

		transaction.commit();

		if (users.isEmpty()) {
			return Optional.empty();
		}

		return Optional.of(users.get(0));
	}

	public void createUser(User user) {
		Session session = sf.getCurrentSession();
		Transaction transaction = session.beginTransaction();
		session.save(user);

		transaction.commit();

	}
}
