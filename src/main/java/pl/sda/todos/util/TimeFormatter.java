package pl.sda.todos.util;

public class TimeFormatter {

	public static String toHours(int minutes) {
		int h = minutes / 60;
		int m = minutes % 60;
		return String.format("%d:%02d", h, m);
	}
}
