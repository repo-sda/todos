package pl.sda.todos.util;

import org.mindrot.jbcrypt.BCrypt;

public class CryptUtil {

	private static final String SALT = BCrypt.gensalt();

	public static String hashPassword(String plaintext) {
		return BCrypt.hashpw(plaintext, SALT);
	}

	public static boolean checkPassword(String plaintext, String encryptedText) {
		return BCrypt.checkpw(plaintext, encryptedText);
	}
}
