package pl.sda.todos.util;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

@Slf4j
//@WebListener
public class HibernateSessionFactoryListener implements ServletContextListener {


	public void contextDestroyed(ServletContextEvent servletContextEvent) {
		SessionFactory sessionFactory = (SessionFactory) servletContextEvent.getServletContext().getAttribute("SessionFactory");
		if (sessionFactory != null && !sessionFactory.isClosed()) {
			log.info("Closing sessionFactory");
			sessionFactory.close();

			try {
				com.mysql.cj.jdbc.AbandonedConnectionCleanupThread.checkedShutdown();
			} catch (Exception e) {
				e.printStackTrace();
			}

			ClassLoader cl = Thread.currentThread().getContextClassLoader();

			Enumeration<Driver> drivers = DriverManager.getDrivers();
			while (drivers.hasMoreElements()) {
				Driver driver = drivers.nextElement();

				if (driver.getClass().getClassLoader() == cl) {

					try {
						log.info("Deregistering JDBC driver " + driver);
						DriverManager.deregisterDriver(driver);

					} catch (SQLException ex) {
						ex.printStackTrace();
					}

				} else {
					log.info("Not deregistering JDBC driver {} as it does not belong to this webapp's ClassLoader " + driver);
				}
			}
		}
		log.info("Released Hibernate sessionFactory resource");
	}

	public void contextInitialized(ServletContextEvent servletContextEvent) {
		StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();

		// Create MetadataSources
		MetadataSources sources = new MetadataSources(registry);

		// Create Metadata
		Metadata metadata = sources.getMetadataBuilder().build();

		// Create SessionFactory
		SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();
		log.info("SessionFactory created successfully");

		servletContextEvent.getServletContext().setAttribute("SessionFactory", sessionFactory);
		log.info("Hibernate SessionFactory Configured successfully");
	}

}
