package pl.sda.todos.service;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.todos.domain.User;
import pl.sda.todos.repository.FakeUserRepository;
import pl.sda.todos.repository.UserRepository;
import pl.sda.todos.util.HibernateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "usersList", value = "/users")
public class UsersListService extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}

	private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		SessionFactory sf = HibernateUtil.getSessionFactory();

		UserRepository userRepository = new UserRepository(sf);
		List<User> all = userRepository.getAllOrderByCreatedDateDesc();

		req.setAttribute("users", all);

		//transaction.commit();
		//sf.getCurrentSession().close();

		req.getRequestDispatcher("users.jsp").include(req, resp);
	}
}
