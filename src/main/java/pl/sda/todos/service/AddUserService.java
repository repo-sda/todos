package pl.sda.todos.service;

import org.hibernate.SessionFactory;
import pl.sda.todos.domain.User;
import pl.sda.todos.repository.FakeUserRepository;
import pl.sda.todos.repository.UserRepository;
import pl.sda.todos.util.CryptUtil;
import pl.sda.todos.util.HibernateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet(name = "adduser", value = "/adduser")
public class AddUserService extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		SessionFactory sf = HibernateUtil.getSessionFactory();

		String login = req.getParameter("login");
		String password = req.getParameter("password");
		String password2 = req.getParameter("password2");
		String name = req.getParameter("name");
		String surname = req.getParameter("surname");

		String errMsg = "";
		if (login == null || login.isEmpty()) {
			errMsg += "<b>Login</b> nie może być pusty/null.<br>";
		} else {
			login = login.toLowerCase();
		}
		if (password == null || password.isEmpty()) {
			errMsg += "<b>Hasło</b> nie może być puste/null.<br>";
		}
		if (password != null && !password.equals(password2)) {
			errMsg += "<b>Hasła</b> muszą być takie same.<br>";
		}
		if (name == null || name.isEmpty()) {
			errMsg += "<b>Imię</b> nie może być puste/null.<br>";
		}
		if (surname == null || surname.isEmpty()) {
			errMsg += "<b>Nazwisko</b> nie może być puste/null.<br>";
		}

		UserRepository userRepository = new UserRepository(sf);
		Optional<User> userByLogin = userRepository.findByLogin(login);
		if (userByLogin.isPresent()) {
			errMsg += "<b>Login</b> '" + login + "' już istnieje.";
		}

		if (!errMsg.isEmpty()) {
			req.setAttribute("_ERROR", errMsg);
			req.getRequestDispatcher("/newuser").forward(req, resp);
			return;
		}

		User user = new User(login, CryptUtil.hashPassword(password), name, surname);

		userRepository.createUser(user);

		req.setAttribute("_OK", "Dodano użytkownika '" + login + "'");
		req.getRequestDispatcher("/users").forward(req, resp);
	}
}
