package pl.sda.todos.service;

import org.hibernate.SessionFactory;
import pl.sda.todos.domain.Status;
import pl.sda.todos.domain.Task;
import pl.sda.todos.repository.FakeTaskRepository;
import pl.sda.todos.repository.TaskRepository;
import pl.sda.todos.util.HibernateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "tasktransition", value = "/transit")
public class TaskTransitionService extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int id = Integer.parseInt(req.getParameter("id"));
		Status status = Status.valueOf(req.getParameter("to"));

		SessionFactory sf = HibernateUtil.getSessionFactory();

		TaskRepository taskRepository = new TaskRepository(sf);
		Task task = taskRepository.getById(id);

		if (!task.isTransitiableTo(status)) {
			req.setAttribute("_ERROR", "Zadanie '" + task.getTitle() + "' nie może zmienić statusu na " + status);
		} else {
			/// !!!!!!!
			taskRepository.changeTaskStatus(task, status);

			req.setAttribute("_OK", "Zadanie '" + task.getTitle() + "' zmieniło status na " + status);
		}

		req.getRequestDispatcher("/board").forward(req, resp);
	}
}
