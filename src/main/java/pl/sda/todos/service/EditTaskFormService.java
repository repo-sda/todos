package pl.sda.todos.service;

import org.hibernate.SessionFactory;
import pl.sda.todos.domain.Priority;
import pl.sda.todos.domain.Task;
import pl.sda.todos.domain.User;
import pl.sda.todos.repository.FakeTaskRepository;
import pl.sda.todos.repository.FakeUserRepository;
import pl.sda.todos.repository.TaskRepository;
import pl.sda.todos.repository.UserRepository;
import pl.sda.todos.util.HibernateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "edittaskform", value = "/edittask")
public class EditTaskFormService extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int id = Integer.parseInt(req.getParameter("id"));

		SessionFactory sf = HibernateUtil.getSessionFactory();

		TaskRepository taskRepository = new TaskRepository(sf);
		UserRepository userRepository = new UserRepository(sf);

		List<User> users = userRepository.getAll();
		Task task = taskRepository.getById(id);

		int from = 1;
		int to = Priority.values().length;

		req.setAttribute("users", users);
		req.setAttribute("from", from);
		req.setAttribute("to", to);

		req.setAttribute("task", task);

		req.getRequestDispatcher("edittask.jsp").include(req, resp);
	}
}
