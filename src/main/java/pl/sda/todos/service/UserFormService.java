package pl.sda.todos.service;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.todos.domain.User;
import pl.sda.todos.repository.FakeUserRepository;
import pl.sda.todos.repository.UserRepository;
import pl.sda.todos.util.HibernateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "userForm", value = "/user")
public class UserFormService extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int id = Integer.parseInt(req.getParameter("id"));

		// 1)
		SessionFactory sf = HibernateUtil.getSessionFactory();

		//SessionFactory sf = (SessionFactory) getServletContext().getAttribute("SessionFactory");
		//Transaction transaction = sf.getCurrentSession().beginTransaction();

		// 2)
		UserRepository userRepository = new UserRepository(sf);
		//FakeUserRepository userRepository = new FakeUserRepository();

		req.setAttribute("user", userRepository.getById(id));

		//transaction.commit();
		//sf.getCurrentSession().close();

		req.getRequestDispatcher("user.jsp").include(req, resp);
	}
}
