package pl.sda.todos.service;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.todos.domain.Status;
import pl.sda.todos.domain.Task;
import pl.sda.todos.repository.FakeTaskRepository;
import pl.sda.todos.repository.TaskRepository;
import pl.sda.todos.repository.UserRepository;
import pl.sda.todos.util.HibernateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "board", value = "/board")
public class BoardService extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}

	private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//SessionFactory sf = (SessionFactory) getServletContext().getAttribute("SessionFactory");
		//Transaction transaction = sf.getCurrentSession().beginTransaction();

		//TaskRepository taskRepository = new TaskRepository(sf);
		SessionFactory sf = HibernateUtil.getSessionFactory();
		TaskRepository taskRepository = new TaskRepository(sf);

		List<Task> todo = taskRepository.getAllByStatusOrderByPriority(Status.TODO);
		List<Task> doing = taskRepository.getAllByStatusOrderByPriority(Status.DOING);
		List<Task> done = taskRepository.getAllByStatusOrderByPriority(Status.DONE);

		req.setAttribute("todo", todo);
		req.setAttribute("doing", doing);
		req.setAttribute("done", done);

		//req.setAttribute("_ERROR", "coś poszło nie tak!");
		//req.setAttribute("_OK", "Jest extra hurraaaaa");

		//transaction.commit();
		//sf.getCurrentSession().close();

		req.getRequestDispatcher("board.jsp").include(req, resp);
	}
}
