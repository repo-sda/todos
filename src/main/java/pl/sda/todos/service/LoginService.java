package pl.sda.todos.service;

import org.hibernate.SessionFactory;
import pl.sda.todos.domain.User;
import pl.sda.todos.repository.FakeUserRepository;
import pl.sda.todos.repository.UserRepository;
import pl.sda.todos.util.CryptUtil;
import pl.sda.todos.util.HibernateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

@WebServlet(name = "login", value = "/login")
public class LoginService extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String login = req.getParameter("login");
		String password = req.getParameter("password");

		String errMsg = "";
		if (login == null || login.isEmpty()) {
			errMsg += "<b>Login</b> nie może być pusty/null.<br>";
		} else {
			login = login.toLowerCase();
		}
		if (password == null || password.isEmpty()) {
			errMsg += "<b>Hasło</b> nie może być puste/null.<br>";
		}

		SessionFactory sf = HibernateUtil.getSessionFactory();
		UserRepository userRepository = new UserRepository(sf);

		User user = null;
		Optional<User> byLogin = userRepository.findByLogin(login);
		if (!byLogin.isPresent()) {
			errMsg += "Brak użytkownika <b>" + login + "</b>.";
		} else {
			user = byLogin.get();
			if (!CryptUtil.checkPassword(password, user.getPassword())) {
				errMsg += "Niepoprawne hasło.";
			}
		}

		if (!errMsg.isEmpty()) {
			req.setAttribute("_ERROR", errMsg);
			req.getRequestDispatcher("/").forward(req, resp);
			return;
		}

		HttpSession session = req.getSession();
		session.setAttribute("loggedUserId", user.getId());
		session.setAttribute("loggedUserLogin", user.getLogin());

		req.setAttribute("_OK", "Poprawnie zalogowany jako <b>" + user.getLogin() + "</b>");
		req.getRequestDispatcher("/board").forward(req, resp);
	}
}
