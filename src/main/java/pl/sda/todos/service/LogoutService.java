package pl.sda.todos.service;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "logout", value = "/logout")
public class LogoutService extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		if (session.getAttribute("loggedUserLogin") != null) {
			session.removeAttribute("loggedUserLogin");
			session.removeAttribute("loggedUserId");

			req.setAttribute("_OK", "Zostałeś wylogowany");
		}
		req.getRequestDispatcher("/").forward(req, resp);
	}
}
