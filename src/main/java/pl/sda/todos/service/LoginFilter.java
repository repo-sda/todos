package pl.sda.todos.service;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(filterName = "loginfilter", urlPatterns = "/*")
public class LoginFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		String servletPath = ((HttpServletRequest) request).getServletPath();
		boolean logged = ((HttpServletRequest) request).getSession().getAttribute("loggedUserLogin") != null;

		// jesli zalogowany
		if (logged) {
			// a wchodzi na '/' lub '/login'
			if (servletPath.equals("/") || servletPath.equals("/login") || servletPath.equals("/index")) {
				// przekierowanie na '/board'
				((HttpServletResponse) response).sendRedirect("/board");
			}
			// normalne przetwarzanie
			chain.doFilter(request, response);
		} else {
			// moze wejsc na strony logowania i logike
			if (servletPath.equals("/") || servletPath.equals("/login") || servletPath.equals("/index")) {
				chain.doFilter(request, response);
			} else {
				((HttpServletResponse) response).sendRedirect("/");
			}
		}
	}

	@Override
	public void destroy() {

	}
}
