package pl.sda.todos.service;

import org.hibernate.SessionFactory;
import pl.sda.todos.domain.Priority;
import pl.sda.todos.domain.Status;
import pl.sda.todos.domain.Task;
import pl.sda.todos.domain.User;
import pl.sda.todos.repository.*;
import pl.sda.todos.util.HibernateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "addtask", value = "/addtask")
public class AddTaskService extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		SessionFactory sf = HibernateUtil.getSessionFactory();

		TaskRepository taskRepository = new TaskRepository(sf);
		UserRepository userRepository = new UserRepository(sf);

		String title = req.getParameter("title");
		String description = req.getParameter("description");
		int priority = Integer.parseInt(req.getParameter("priority"));
		int userId = Integer.parseInt(req.getParameter("user"));

		User user = userRepository.getById(userId);

		String errMsg = "";
		if (title == null || title.isEmpty()) {
			errMsg += "<b>Tytuł</b> nie może być pusty/null.<br>";
		}
		if (description == null || description.isEmpty()) {
			errMsg += "<b>Opis</b> nie może być pusty/null.<br>";
		}
		if (priority < 1 || priority > 5) {
			errMsg += "<b>Priorytet</b> musi być od 1 do 5.<br>";
		}
		if (user == null) {
			errMsg += "<b>Użytkownik</b> musi istnieć.<br>";
		}

		if (!errMsg.isEmpty()) {
			req.setAttribute("_ERROR", errMsg);
			req.getRequestDispatcher("/newtask").forward(req, resp);
			return;
		}

		Task task = new Task(title, description, Priority.get(priority), Status.TODO, user);
		//user.getTasks().add(task);

		taskRepository.createTask(task);

		req.setAttribute("_OK", "Dodano Task '" + task.getTitle() + "'");

		req.getRequestDispatcher("/board").forward(req, resp);

	}
}
