package pl.sda.todos.service;

import org.hibernate.SessionFactory;
import pl.sda.todos.domain.Priority;
import pl.sda.todos.domain.User;
import pl.sda.todos.repository.FakeUserRepository;
import pl.sda.todos.repository.UserRepository;
import pl.sda.todos.util.HibernateUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "newtask", value = "/newtask")
public class NewTaskFormService extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}

	private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		UserRepository userRepository = new UserRepository(sf);

		List<User> users = userRepository.getAll();
		int from = 1;
		int to = Priority.values().length;

		req.setAttribute("users", users);
		req.setAttribute("from", from);
		req.setAttribute("to", to);

		req.getRequestDispatcher("newtask.jsp").include(req, resp);
	}
}
